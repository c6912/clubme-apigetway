const config = {
    port: process.env.PORT || 9090,
    jwtSecret: process.env.JWT_SECRET || 'dorco',
    authServiceUrl:process.env.AUTH_SERVICE_URL || 'http://localhost:2400',
    webScannerServiceUrl:process.env.WEBSCANNER_SERVICE_URL || 'http://localhost:2500',
    CommonServiceUrl:process.env.COMMON_SERVICE_URL || 'http://localhost:2600',
}
module.exports = { config }