const { config } = require("./config")

const publicRoutes = [{
    method: 'POST',
    url: '/users/auth/register',

},
{
    method: 'POST',
    url: '/users/auth/login'
}]

const proxyRoutes = [{
    path: '/users',
    url: config.authServiceUrl
},
{
    path: '/webscanner',
    url: config.webScannerServiceUrl
},
{
    path: '/common',
    url: config.CommonServiceUrl
}]

module.exports = { publicRoutes, proxyRoutes }