const express = require('express')
const cors = require('cors')
const { publicRoutes, proxyRoutes } = require('./routes')
const { config } = require('./config')
const jwt = require('jsonwebtoken')
const httpProxy = require('http-proxy')

const proxy = httpProxy.createProxyServer({ secure: false, changeOrigin: true })
const app = express()

app.use(cors())

app.use(async (req, res, next) => {
    const isPublicRoute = publicRoutes.some((route) => route.url.toLocaleLowerCase() === req.url.toLocaleLowerCase() && route.method.toLocaleLowerCase() === req.method.toLocaleLowerCase())
    if (isPublicRoute) return next()
    try {
        const token = req.headers.authorization?.replace('Bearer', '')?.trim()
        if (!token) return res.status(401).send()

        const jwtData = await jwt.verify(token, config.jwtSecret)
        req.headers['clubme-user-id'] = jwtData.userId;

        return next()
    } catch (err) {
        console.log(err)
        return res.status(401).send()
    }
})

app.use((req, res) => {
    const route = proxyRoutes.find((route) => req.url.startsWith(route.path))
    if (!route) return res.status(401)

    const proxyUrl = route.url;
    req.url = req.url.replace(route.path, '');
    try {
        proxy.web(req, res, {
            target: proxyUrl,
            prependPath: true
        })
    } catch (error) {
        console.log(error);
        return res.status(401).send()
    }
})

app.listen(config.port, () => console.log(`API Gateway started and listening on port ${config.port}`))
